import QtQuick
import QtQuick.Controls
import QtQuick.Dialogs

Window {
	visible: true
	Button {
		id: button
		onClicked: fileDialog.open()
		text: "Click me"
	}
	
	FileDialog {
	    id: fileDialog
	    onAccepted: {
	    	button.text = selectedFile.toString();
	    }
	}
}

